// 1. Запитайте у користувача два числа. Перевірте, чи є кожне з введених значень числом. 
// Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом. 
// Виведіть на екран всі числа від меншого до більшого за допомогою циклу for.

let number1;
let number2;

do {
  number1 = prompt('Введіть перше число:');
  if (isNaN(number1)) {
    alert('Ви ввели не число. Введіть число будь ласка.');
  }
} while (isNaN(number1));

do {
  number2 = prompt('Введіть друге число:');
  if (isNaN(number2)) {
    alert('Ви ввели не число. Введіть число будь ласка.');
  }
} while (isNaN(number2));

number1 = parseFloat(number1);
number2 = parseFloat(number2);

let start = Math.min(number1, number2);
let end = Math.max(number1, number2);

for (let i = start; i <= end; i++) {
  console.log(i);
}



// 2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом. 
// Якщо введене значення не є парним числом, то запитуйте число доки користувач не введе правильне значення.

let number;

do {
  number = prompt('Введіть число:');
  if (isNaN(number)) {
    alert('Ви ввели не число. Введіть число будь ласка.');
  } else if (number % 2 !== 0) {
    alert('Ви ввели непарне число. Введіть парне число будь ласка.');
  }
} while (isNaN(number) || number % 2 !== 0);

console.log('Ви ввели парне число:', number);